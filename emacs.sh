sudo apt-get -y install texinfo libx11-dev libxpm-dev libjpeg-dev libpng-dev libgif-dev libtiff-dev libgtk2.0-dev libncurses-dev
wget ftp://ftp.gnu.org/pub/gnu/emacs/emacs-25.2.tar.gz
tar -zxvf emacs-25.2.tar.gz
cd emacs-25.2/
./configure
make
sudo make install
